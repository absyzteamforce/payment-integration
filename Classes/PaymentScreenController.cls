public without sharing class PaymentScreenController {
    
    public String userId; //Getting the userId from URL.
    public String userIdtopage {get;set;} //Sending the userId from controller to Page.
    public Retail_User__c loggedInUser {get;set;}       // User in Context
    public Orderspendingpayment__c selOrder {get;set;}  // Order in Context
    public Boolean error; // To check the error status
    public Boolean displayMessages {get;set;}  // Message to display Messages
    public String hashtopagenew {get;set;} //sendinghashtopage
    public String txnidtopage {get;set;} // Transaction Id for the payment is generated in the controller with required format and sent to the page.
    public String lnametopage {get;set;} // Last name of the user is captured from the object and sent to page.
    public String fnametopage {get;set;} // First name of the user is captured from the object and sent to page.
    public String productinfotopage {get;set;} // Product Info is the service or product id for the payment is sent to page.
    public String cnttopage {get;set;}  // Phone number is captured and then sent to page.
    public String emailtopage {get;set;} // Email is captured and theen sent to page.
    public Decimal finalamounttopayu {get;set;}//to be used in payumoney production server
    public Decimal finalamounttopayuint {get;set;}//Integer of final amount for test server
    public String responsefrompayu {get;set;} //payment status from payu
    public String paymentidfrompage {get;set;} //payment id from payu
    public String meridfrompage {get;set;} //payment id from payu
    public retail_order__c rtlOrdernew {get;set;} // On successful payment, we create a new retail order record.
    public Applicant__c appnew {get;set;}// On successful payment, applicant record is also created and mapped to retail order.
    public String errcodetopage {get;set;} // error code to display on page.
    public String errmsgtopage {get;set;} // error message to display on page.
    public String unmappedstatusfrompayment {get;set;} // parameter from payumoney to store cancel status
    public String field9frompayment {get;set;} // user cancelled parameter from payumoney
    public String customlabeltopage {get;set;} // customlabel to page for Eazyvisaapptestwithamazonorderstyle.
    public String customLabel = System.Label.Paymentreturnpage; // custom label for Eazyvisaapptestwithamazonorderstyle.
    public String customLabelpmnt = System.Label.Payment_Screen; // custom label for payment screen page.
    public String customLabelpmntsuccessmsgtopage {get;set;} // success messagetopage
    public String customLabelpmntsuccessmsg = System.Label.PaymentSuccessMessage; // custom label for payment success message.
    public String customLabelpmnttxnidtopage {get;set;} // success messagetopage
    public String customLabelpmnttxnid = System.Label.payment_transaction_id; // custom label for payment success message.
    public String customLabelpmntfailuremsgtopage {get;set;} // failure messagetopage
    public String customLabelpmntfailuremsg = System.Label.Payment_Failure_Message; // custom label for payment failure message.
    public String customLabelpmntcancelmsgtopage {get;set;} // cancel messagetopage
    public String customLabelpmntcancelmsg = System.Label.Payment_Cancel_Message; // custom label for payment cancel message.
    public String customLabelpmntorderdetailsmsgtopage {get;set;} // order details messagetopage
    public String customLabelpmntorderdetailsmsg = System.Label.View_Order_Details_Message; // custom label for order details message.
    public string phno{get; set;} // Formatting the phone number as per payumoney format.
    public String customLabelkeyvaluetopage {get;set;} // key value to page.
    public String customLabelkey = System.Label.Key_value_for_Payment; // custom label for Key value.
    public String customLabelsaltvaluetopage {get;set;} // salt value to page.
    public String customLabelsalt = System.Label.Salt_value_for_Payment; // custom label for salt value.
    public String customLabelproductinfovaluetopage {get;set;} // product info value to page.
    public String customLabelproductinfo = System.Label.Product_Info_for_Payment; // custom label for product info value.
    
    //CONSTRUCTOR
    public PaymentScreenController() {
        
        
        
        loggedInUser = new Retail_User__c(); // To know, who is logged in retail user, in the context of eazyvisa project.
        error = false;
        displayMessages = false;
        rtlOrdernew = new retail_order__c();
        appnew = new Applicant__c();
        customlabeltopage = customLabelpmnt;
        customLabelpmntsuccessmsgtopage = customLabelpmntsuccessmsg;
        customLabelpmnttxnidtopage = customLabelpmnttxnid;
        customLabelkeyvaluetopage = customLabelkey;
        customLabelsaltvaluetopage = customLabelsalt;
        customLabelproductinfovaluetopage = customLabelproductinfo;
        customLabelpmntfailuremsgtopage = customLabelpmntfailuremsg;
        customLabelpmntcancelmsgtopage = customLabelpmntcancelmsg;
        customLabelpmntorderdetailsmsgtopage = customLabelpmntorderdetailsmsg;
        
	//Start -- Response parameters that are received from Payumoney.
        responsefrompayu = ApexPages.currentPage().getParameters().get('status');
        paymentidfrompage = ApexPages.currentPage().getParameters().get('payuMoneyId');
        meridfrompage = ApexPages.currentPage().getParameters().get('mihpayid');
        errcodetopage = ApexPages.currentPage().getParameters().get('error');
        errmsgtopage = ApexPages.currentPage().getParameters().get('error_Message');
        unmappedstatusfrompayment = ApexPages.currentPage().getParameters().get('unmappedstatus');
        field9frompayment = ApexPages.currentPage().getParameters().get('field9');
	//End -- Response parameters that are received from Payumoney.
        
    }

    //Involed from doAction method on Page Load
    //fetchUserDetails os required in EazyVisa for getting the UserId to land on to the particular user record.	
    public void fetchUserDetails(){
        if(userId!=null){
            List<Retail_User__c> lstUser = [select first_name__c, name, email__c, contact_number__c, Email_Notification_Alerts__c, 
                    (select name, Contact_Number__c, Country_of_Birth__c, Current_Address__c, Date_of_Birth__c, 
                    Email__c, First_Name__c, Gender__c, Last_Name__c, Nationality__c, Passport_Expiry_Date__c, 
                    Passport_Number__c, Pincode__c from applicants__r), (select name from Retail_Order__r) 
                    from retail_user__c where id =: userId LIMIT 1];
            if(lstUser!=null && lstUser.size()>0){
                loggedInUser = lstUser[0];
                
        
            }
            else{
                //invalid session or user
                error = true;
            }
        }
    }

    //Invoked on page load
    //Encrypts the userID and displays on the page URL.
    //Simultaneously, fetches the the required details to be sent for payment method by calling "fetchOrderDetails" method.	
    public pageReference doAction(){
        //insert custom setting
         
        //Original Code
        String loggedInPortalUserId = null;
        if(AuthenticationManager.getLoggedInUserType() != null){
            loggedInPortalUserId = AuthenticationManager.getLoggedInUserDetails();
            System.debug('---Heloo--'+loggedInPortalUserId);
            userId = loggedInPortalUserId;
            
            //userIdtopage = userId;
            
            
            //Encryption of userID to avoid URL hacking
                Blob data = Blob.valueOf(userId);
                Blob cryptoKey = Blob.valueOf('380DB410E8B11FA91234567891234567');
                Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey , data );
                String b64Data = EncodingUtil.base64Encode(encryptedData);
                userIdtopage = b64Data ;
                
            
            //userIdtopage = userId;
            fetchUserDetails();
            
	    //If there is no error, then query the required details using fetchOrderDetails method.	
	    if(!error){
                fetchOrderDetails();
            }
        }
        
        
       
       
        //Redirect user to login screen in case of logouts
        if(loggedInPortalUserId == null || error){       
            return AuthenticationManager.logout();       
        }
        return null;
        
        
        
        
        return null;
        
        
    }

    //Invoked on Page Load from doAction on successful session available
    //fetchOrderDetails method is used to get the record details of a record and then these details are sent to the payment information.
    //Objects used here are Orderspendingpayment__c, Retail_Order__c, Applicant__c, these are in context withh Eazyvisa.
    //purpose of this method is to fetch the input parameters for payumoney like First Name, Phone number, Email, and mandatory or optional parameters mentioned in VF page of payment code.		
    public void fetchOrderDetails()
    {
        //FETCH THE ORDER DETAILS AND PREPARE FOR PAYMENT
        if(ApexPages.currentPage().getParameters().containsKey('orderId'))
        {
            String orderId = ApexPages.currentPage().getParameters().get('orderId');
            List<Orderspendingpayment__c> lstOrders = [select name, order_status__c, destinationcountry__c,
                    Order_Amount__c, visa_fee__c, embassy_partner_fee__c, Taxes__c, visatype__c,
                    firstname__c, lastname__c , Email__c,Cancel_Status__c,
                    gender__c, pincode__c, Nationality__c,
                    Country_of_Birth__c, dateofbirth__c, Passport_Number__c,Arrival_Date__c,Departure_Date__c,Cancellation_message__c,
                    Current_Address__c, Contact_number__c,Retail_User__c,Payment_ID__c,Merchant_Order_ID__c,Payment_Error_Code__c,Payment_Error_Message__c,
                    Eazyvisa_com_Service_Fee__c,Total_amount__c,Payment_gateway_charges__c,Net_payable_amount__c,Eazyvisa_com_Service_Fee_Tax__c,development_purpose__c
                    FROM Orderspendingpayment__c where id =: orderId 
                    AND (order_status__c = 'Payment Pending' OR order_status__c = 'Payment Failure' OR order_status__c = 'Payment Success' OR order_status__c = 'Customer pending' OR order_status__c = 'Payment Cancelled') 
                    AND retail_user__c =: userId LIMIT 1
                ];
                system.debug('chkkkk'+orderId+'listtttt'+lstOrders);
                system.debug('userIdinsellorderis: '+userId);               
            if(lstOrders!=null && lstOrders.size()>0)
            {
                selOrder = lstOrders[0];
                phno = selOrder.Contact_number__c;
                phno = phno.replace('-', '');
                phno = phno.replace(' ', '');
                phno = phno.replace(')', '');
                phno = phno.replace('(', '');
                
              if(responsefrompayu == 'success' && (selOrder.Order_Status__c == 'Payment Pending' || selOrder.Order_Status__c == 'Customer pending' || selOrder.Order_Status__c == 'Payment Failure' || selOrder.Order_Status__c == 'Payment Cancelled'))
                {
                
                system.debug('hereinside');
                        system.debug('selorderis::'+selOrder);
                       
                        selOrder.Payment_ID__c = paymentidfrompage;
                        selOrder.Merchant_Order_ID__c = meridfrompage;
                        selOrder.Order_Status__c = 'Payment Success';
                        
                         if(selOrder.Order_Status__c <> 'Payment Pending' || selOrder.Order_Status__c <> 'Customer pending')
                         {  
                         selOrder.Cancellation_message__c = field9frompayment ;
                         }
                        if(responsefrompayu == 'success')
                        {
                        selOrder.Cancel_Status__c = '';
                        selOrder.Cancellation_message__c = '';
                        }
                        
                        selOrder.development_purpose__c = true;
                        system.debug('FIIIIInal'+selOrder);
                        update selOrder;
                        
                        system.debug('selorderorderstatusis:::'+selOrder.Order_Status__c);
                        
                        rtlOrdernew.first_name__c = selOrder.FirstName__c;
                        rtlOrdernew.Last_Name__c = selOrder.LastName__c;
                        rtlOrdernew.gender__c = selOrder.Gender__c;
                        rtlOrdernew.ApplicantEmail__c = selOrder.Email__c;
                        rtlOrdernew.Contact_number__c = selOrder.Contact_Number__c;
                        rtlOrdernew.Nationality__c = selOrder.Nationality__c;
                        rtlOrdernew.Date_of_Birth__c = selOrder.dateofbirth__c;
                        rtlOrdernew.Arrival_Date__c = selOrder.Arrival_Date__c;
                        rtlOrdernew.Departure_Date__c = selOrder.Departure_Date__c;
                        rtlOrdernew.Passport_Expiry_Date__c = selOrder.Passport_Expiry_Date__c;
                        rtlOrdernew.Country_of_Birth__c = selOrder.Country_of_Birth__c;
                        rtlOrdernew.Passport_Number__c = selOrder.Passport_Number__c;
                        rtlOrdernew.Current_Address__c = selOrder.Current_Address__c;
                        rtlOrdernew.pincode__c = selOrder.Pincode__c;
                        rtlOrdernew.destination_country__c = selOrder.DestinationCountry__c;
                        rtlOrdernew.Visa_Type__c = selOrder.VisaType__c;
                        rtlOrdernew.Eazyvisa_com_Service_Fee__c = selOrder.Eazyvisa_com_Service_Fee__c;
                        rtlOrdernew.Eazyvisa_com_Service_Fee_Tax__c = selOrder.Eazyvisa_com_Service_Fee_Tax__c;
                        rtlOrdernew.Payment_gateway_charges__c = selOrder.Payment_gateway_charges__c;
                        rtlOrdernew.Net_payable_amount__c = selOrder.Net_payable_amount__c;
                        rtlOrdernew.Retail_User__c = selOrder.Retail_User__c;   
                        rtlOrdernew.visa_fee__c = selOrder.visa_fee__c;
                        rtlOrdernew.embassy_partner_fee__c = selOrder.embassy_partner_fee__c;
                        rtlOrdernew.Payment_ID__c = selOrder.Payment_ID__c;
                        rtlOrdernew.Merchant_Order_ID__c = selOrder.Merchant_Order_ID__c;
                        rtlOrdernew.Retail_User__c = selOrder.Retail_User__c;
                        rtlOrdernew.Order_Status__c = 'Placed';
                        
                        upsert rtlOrdernew;
                        
                        rtlOrdernew.Retail_Application__c = selOrder.Id;
                        update rtlOrdernew;
                           
                        system.debug('selOrderIdis: '+selOrder.Id);
                        system.debug('retailorderidis: '+rtlOrdernew.id);
                        system.debug('retailordernameis: '+rtlOrdernew.name);
                            
                    }
                            
                    if(responsefrompayu == 'failure')
                    {
                        selOrder.Payment_ID__c = paymentidfrompage;
                        selOrder.Merchant_Order_ID__c = meridfrompage;
                        selOrder.Order_Status__c = 'Payment Failure';
                        selOrder.Payment_Error_Message__c = errmsgtopage;
                        selOrder.Payment_Error_Code__c = errcodetopage;
                        upsert selOrder;
                    }
                    
                    if(responsefrompayu == 'failure' && unmappedstatusfrompayment=='userCancelled' )
                    {
                        system.debug('ENtered into failuree');
                        selOrder.Payment_ID__c = paymentidfrompage;
                        selOrder.Merchant_Order_ID__c = meridfrompage;
                        selOrder.Order_Status__c = 'Payment Cancelled';
                        selOrder.Payment_Error_Message__c = errmsgtopage;
                        selOrder.Payment_Error_Code__c = errcodetopage;
                        selOrder.Cancellation_message__c = field9frompayment ;
                        selOrder.Cancel_Status__c = unmappedstatusfrompayment;
                        try{
                        upsert selOrder;
                        }catch(exception e){
                            
                        }
                    }
                    
                  
                    
                  if(selOrder.Order_Status__c == 'Payment Pending' || selOrder.Order_Status__c == 'Payment Failure' || selOrder.Order_Status__c == 'Payment Cancelled' || selOrder.Order_Status__c == 'Customer pending')
                    
                     
                    {

                        Integer len = 30;
                        Blob txnidgen = crypto.generateAesKey(128);
                        String keytxnidgen = EncodingUtil.convertToHex(txnidgen);
                        String txnid = keytxnidgen.substring(0, len);
                        txnidtopage = txnid;
                        System.debug('value in txnid is ' + txnid);
                                
                        String productinfo = 'ProductInfo';
                        productinfotopage = productinfo;
                        finalamounttopayu = selOrder.Net_payable_amount__c;
                        finalamounttopayuint = Integer.valueof(finalamounttopayu); //In sandbox
                        //finalamounttopayuint = finalamounttopayu; //In production
                       
                        fnametopage = selOrder.firstname__c;
                        
                        String fname;
                        fname = fnametopage;
                       
                        lnametopage = selOrder.lastname__c;
                        
                        String lname;
                        lname = lnametopage;
                        
                        cnttopage = selOrder.Contact_number__c;
                        
                        String cntname;
                        cntname = cnttopage;
                        
                        emailtopage = selOrder.Email__c;
                        
                        String email;
                        email = emailtopage;
                        
                        String concat = (customLabelkey + '|' + txnid + '|' + finalamounttopayuint + '|' + productinfo + '|' + fname + '|' + email + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + customLabelsalt);
                                
                        String concattxnid = (customLabelkey + '|' + '|' + fname + '|' + '|' + '|' + '|' + '|' + '|' + '|' + '|' + customLabelsalt);
                                
                       Blob targetBlob = Blob.valueOf(concat); // calculated keynowkeynowlast
                                
                       Blob hash = Crypto.generateDigest('SHA-512', targetBlob);
                       String hashconverted = EncodingUtil.convertToHex(hash);
                       hashtopagenew = hashconverted;
                       system.debug('valueinhashconverted ' + hashconverted);
                   
                    }
                }
            }
            
       else
       {
            displayMessages = true; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'Something wrong happened. Please try again or contact your system administrator !!'));
        }
    }

    //Logout Method
    public PageReference logout() {

        return AuthenticationManager.logout_website();
    }
    
    //Used in context of EazyVisa for refreshing the page.
    public pageReference refreshPage(){
        ssssssss
        PageReference pg = Page.EazyVisaApp;   
        pg.getParameters().put('Type', 'retail');
        pg.getParameters().put('userID', userIdtopage);
        pg.setRedirect(true);
        return pg;
        
    }
    
    //Used in context of Eazyvisa on a button click.
    public pageReference checkOrderDetails(){
        
        PageReference pg = Page.EazyVisaApp;   
        pg.getParameters().put('Type', 'retail');
        pg.getParameters().put('userID', userIdtopage);
        pg.getParameters().put('order', '1');
        pg.setRedirect(true);
        return pg;
        
    }
    
}